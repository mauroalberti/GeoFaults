module quaternions_wrapper

    use iso_c_binding, only: c_bool, c_double
    use quaternions

    implicit none

contains

    !--------------------------

    subroutine qproduct(quat1, quat2, product) bind(c, name='qproduct')

        ! QUATq_product
        ! quaternion product
        ! Avenue created: 2005-02-11

        type(quaternion) :: quat1, quat2, product

        product = q_product(quat1,quat2)

    end subroutine qproduct

    !--------------------------

    subroutine quat_conjugate(quat, conjugate)

        ! QUATq_conjugate
        ! created 2005-02-10

        type(quaternion) :: quat, conjugate

        conjugate = q_conjugate(quat)

    end subroutine quat_conjugate

    !--------------------------

    subroutine quat_squarednorm(quat, squarednorm)

        !  QUATq_squarednorm
        !  created 2005-02-12

        type(quaternion) :: quat
        real (c_double) :: squarednorm

        squarednorm = q_sqrdnorm(quat)

    end subroutine quat_squarednorm

    !--------------------------

    subroutine quat_scalardivision(quat, scaldiv, quat_scaldiv)

        ! QUATq_divisionbyscalar
        ! quaternion division by a scalar
        ! created: 2005-02-12

        type(quaternion) :: quat, quat_scaldiv
        real (c_double) :: scaldiv

        quat_scaldiv = q_scalardiv(quat, scaldiv)

    end subroutine quat_scalardivision

    !--------------------------

    subroutine quat_inverse(quat, inverse)

        !  quaternion inverse
        !  created: 2005-02-19

        type(quaternion) :: quat, inverse

        inverse = q_inverse(quat)

    end subroutine quat_inverse

    !--------------------------

    subroutine is_quat_normalized(quat, is_normalized)

        ! QUATq_normalizedquaterniontest
        ! created 2005-02-12

        type(quaternion) :: quat
        logical(c_bool) :: is_normalized

        is_normalized = q_is_normalized(quat)

    end subroutine is_quat_normalized

    !--------------------------

    subroutine quat_normalization(quat, normalized)

        !  QUATq_normalization
        !  transformation from quaternion to normalized quaternion
        !  created: 2005-02-14

        type(quaternion) :: quat, normalized

        normalized = q_normalized(quat)

    end subroutine quat_normalization

    !--------------------------
    ! calculates a quaternion from a 3x3 matrix

    subroutine quaternfromcartmatr(focmec_matrix, quat)

        ! QUATq_TPBcartesmatrix2quaternion
        ! modified 2005-02-17

        real (c_double) :: focmec_matrix(3, 3)
        type(quaternion) :: quat

        quat = q_from_matrix(focmec_matrix)

    end subroutine quaternfromcartmatr

    !--------------------------

end module quaternions_wrapper