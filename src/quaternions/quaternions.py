# adapted from: https://stackoverflow.com/questions/19263879/speeding-up-element-wise-array-multiplication-in-python
# answer by: Alexander Vogt

from ctypes import CDLL, POINTER, c_double
import numpy as np
import time

fortran = CDLL("./libquats.so")

fortran.qproduct.argtypes = [
    POINTER(c_double),
    POINTER(c_double),
    POINTER(c_double)
]

a = np.empty(4, dtype=c_double)
b = np.empty(4, dtype=c_double)
c = np.empty(4, dtype=c_double)

print(c)

a[:] = np.random.rand(4)
b[:] = np.random.rand(4)

start = time.time()

fortran.qproduct(
    a.ctypes.data_as(POINTER(c_double)),
    b.ctypes.data_as(POINTER(c_double)),
    c.ctypes.data_as(POINTER(c_double))
)

end = time.time()

print(c)

print('Fortran took ', end - start, 'seconds')
