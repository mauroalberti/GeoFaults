# GeoFaults

Fortran tools for meso-fault analysis.

It derives from two programs (checks folder in 'original'):
**ForwardStress** calculates the theoretical stress components given provided fault orientations.
**FaultCorrelation** calculates the coherence (correlation) between fault attitudes.
originally published in:
Alberti, M., 2010. Analysis of kinematic correlations in faults and focal mechanisms with GIS and Fortran programs. Computers and Geosciences 36, 186-194.

Currently a small subset of the functionalities - quaternion processing - has been converted into a module that can be compiled into a shared library and loaded from Python using ctypes.
As of 2021-10-24 only quaternion product has been exposed to Python.

Status:
alpha.